package de.swm.mj.smlreader.domain;

import lombok.Data;

@Data
public class MeterData {

    private String meterId;
    private long timestamp;
    private double energy;
    private double power;
    private double powerL1;
    private double powerL2;
    private double powerL3;
}
