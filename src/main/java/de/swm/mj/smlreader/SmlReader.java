/*
 * Copyright 2016 SWM Services GmbH
 */

package de.swm.mj.smlreader;

import lombok.extern.slf4j.Slf4j;

import java.util.Scanner;

import static de.swm.mj.smlreader.SerialPortSmlAdapter.Mode;

@Slf4j
public class SmlReader {


    public static void main(String[] args) {
        System.out.println("Select mode:");
        System.out.println("1) Read from COM port");
        System.out.println("2) Read from file");
        System.out.println("3) Write from COM port to file");

        System.out.print(">> ");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        Mode mode;
        switch (choice) {
            case 1:
                mode = Mode.READ_FROM_COM_PORT;
                break;
            case 2:
                mode = Mode.READ_FROM_FILE;
                break;
            case 3:
                mode = Mode.WRITE_TO_FILE;
                break;
            default:
                mode = Mode.READ_FROM_COM_PORT;
        }

        SerialPortSmlAdapter adapter = new SerialPortSmlAdapter(mode);
        adapter.run();
    }
}
