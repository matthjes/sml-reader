/*
 * Copyright 2017 SWM Services GmbH
 */

package de.swm.mj.smlreader;

public class ClientException extends Exception {

	public ClientException(String message) {
		super(message);
	}



	public ClientException(String message, Throwable cause) {
		super(message, cause);
	}
}
