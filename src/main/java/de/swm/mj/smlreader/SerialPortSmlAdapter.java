/*
 * Copyright 2017 SWM Services GmbH
 */

package de.swm.mj.smlreader;

import de.swm.mj.smlreader.domain.MeterData;
import gnu.io.NRSerialPort;
import lombok.extern.slf4j.Slf4j;
import org.openmuc.jsml.structures.*;
import org.openmuc.jsml.structures.responses.SmlGetListRes;
import org.openmuc.jsml.transport.MessageExtractor;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


@Slf4j
public class SerialPortSmlAdapter {

    public enum Mode {
        WRITE_TO_FILE, READ_FROM_FILE, READ_FROM_COM_PORT
    }

    private static final String HOME = System.getProperty("user.home");

    private volatile boolean isRunning = true;
    private final Mode mode;

    private static final Map<String, ValueParser> VALUE_PARSER_MAP = new HashMap<>();

    static {
        VALUE_PARSER_MAP.put("1-0:96.1.0*255", (o, i) -> i.setMeterId(getMeterId(((OctetString) o).getValue())));
        VALUE_PARSER_MAP.put("1-0:1.8.0*255", (o, i) -> i.setEnergy(getValue(o) / 10.));
        VALUE_PARSER_MAP.put("1-0:16.7.0*255", (o, i) -> i.setPower(getValue(o) / 100.));
        VALUE_PARSER_MAP.put("1-0:36.7.0*255", (o, i) -> i.setPowerL1(getValue(o) / 100.));
        VALUE_PARSER_MAP.put("1-0:56.7.0*255", (o, i) -> i.setPowerL2(getValue(o) / 100.));
        VALUE_PARSER_MAP.put("1-0:76.7.0*255", (o, i) -> i.setPowerL3(getValue(o) / 100.));
    }


    @FunctionalInterface
    private interface ValueParser {
        void parse(ASNObject object, MeterData meterData);
    }

    public SerialPortSmlAdapter(Mode mode) {
        this.mode = mode;
    }

    public void run() {
        String file = HOME + "/meter-data.out";
        log.info("Using home directory: {}", HOME);

        InputStream inputStream;
        try {
            switch (mode) {
                case WRITE_TO_FILE:
                    inputStream = initFromComPort();
                    this.writeToFile(file, inputStream);
                    break;
                case READ_FROM_FILE:
                    inputStream = initFromFile(file);
                    this.run(inputStream);
                    break;
                case READ_FROM_COM_PORT:
                    inputStream = initFromComPort();
                    this.run(inputStream);
            }
        } catch (Exception e) {
            log.error("Exception", e);
        }
    }


    private InputStream initFromComPort() throws IOException {
        return setupComPort();
    }

    private InputStream initFromFile(String filename) throws IOException {
        Path input = Paths.get(filename);
        return new DataInputStream(new BufferedInputStream(Files.newInputStream(input)));
    }


    private InputStream setupComPort() throws IOException {
        if (NRSerialPort.getAvailableSerialPorts()
                .isEmpty()) {
            throw new IOException("No suitable ports found!");
        }

        String selectedPort = null;
        for (String port : NRSerialPort.getAvailableSerialPorts()) {
            log.info("Found port: {}", port);
            selectedPort = port;
        }

        assert selectedPort != null;
        log.info("Selected port: {}", selectedPort);

        int baudRate = 9600;
        NRSerialPort serialPort = new NRSerialPort(selectedPort, baudRate);
        serialPort.connect();
        log.info("Connected to port: {}", selectedPort);

        serialPort.notifyOnDataAvailable(true);

        return serialPort.getInputStream();

    }


    private void run(InputStream inputStream) {
        log.info("Starting receiver thread");
        DataInputStream dataInputStream = new DataInputStream(new BufferedInputStream(inputStream));
        while (isRunning) {
            createMessageExtractor(dataInputStream)
                    .ifPresent(extractor -> createDataInputStream(extractor).ifPresent(this::processStream));
        }
    }

    private void writeToFile(String filename, InputStream inputStream) throws Exception {
        BufferedInputStream bis = new BufferedInputStream(inputStream);
        Path output = Paths.get(filename);

        if (!Files.exists(output)) {
            Files.createFile(output);
        }

        while (true) {
            while (bis.available() > 0) {
                int available = bis.available();
                byte[] bytes = new byte[available];
                int read = bis.read(bytes, 0, available);
                Files.write(output, bytes, StandardOpenOption.APPEND);
                log.info("{} bytes written to output file: {}", read, filename);
            }
        }

    }


    private Optional<MessageExtractor> createMessageExtractor(DataInputStream dis) {
        try {
            MessageExtractor extractor = new MessageExtractor(dis, 3000);
            return Optional.of(extractor);
        } catch (IOException e) {
            log.error("Could not create SML message extractor: {}", e.getMessage());
            sleep();
            return Optional.empty();
        }
    }


    private Optional<DataInputStream> createDataInputStream(MessageExtractor extractor) {
        try {
            DataInputStream is = new DataInputStream(new ByteArrayInputStream(extractor.getSmlMessage()));
            return Optional.of(is);
        } catch (IOException e) {
            log.error("Could not read SML message: {}", e.getMessage());
            sleep();
            return Optional.empty();
        }
    }


    private void processStream(DataInputStream dis) {
        // SML file = multiple SML messages
        log.debug("Processing data...");
        try {
            while (dis.available() > 0) {
                log.debug("Available bytes: {}", dis.available());
                SmlMessage message = new SmlMessage();

                if (!message.decode(dis)) {
                    throw new IOException("Could not decode message");
                } else {
                    ASNObject obj = message.getMessageBody()
                            .getChoice();

                    if (log.isTraceEnabled()) {
                        log.debug("Object: {}", obj);
                    }

                    MeterData meterData = new MeterData();
                    if (obj instanceof SmlGetListRes) {
                        processMessage((SmlGetListRes) obj, meterData);
                    }
                }

            }
            dis.close();
        } catch (IOException e) {
            log.error("Error while reading / decoding the SML message: {}", e.getMessage());
            sleep();
        }
    }


    private void sleep() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            log.info("Thread interrupted");
            Thread.currentThread()
                    .interrupt();
        }
    }


    private void processMessage(SmlGetListRes res, MeterData meterData) {
        if (log.isTraceEnabled()) {
            log.trace("SML message: {}", res.toString());
        }
        SmlList values = res.getValList();
        SmlListEntry[] entries = values.getValListEntry();
        for (SmlListEntry entry : entries) {
            String obisCode = getObisCode(entry.getObjName()
                    .getValue());
            ASNObject valueObj = entry.getValue()
                    .getChoice();

            if (log.isTraceEnabled()) {
                log.trace("OBIS: {}, Value: {}", obisCode, valueObj.toString());
            }

            VALUE_PARSER_MAP.getOrDefault(obisCode, (o, i) -> {
            })
                    .parse(valueObj, meterData);

            meterData.setTimestamp(Instant.now()
                    .toEpochMilli());
        }

        log.debug("Item: {}", meterData);
    }


    private static Double getValue(ASNObject valueObj) {
        if (valueObj instanceof Unsigned32) {
            return (double) ((Unsigned32) valueObj).getVal();
        } else if (valueObj instanceof Integer8) {
            return (double) ((Integer8) valueObj).getVal();
        } else if (valueObj instanceof Unsigned8) {
            return (double) ((Unsigned8) valueObj).getVal();
        } else if (valueObj instanceof Unsigned16) {
            return (double) ((Unsigned16) valueObj).getVal();
        } else if (valueObj instanceof Unsigned64) {
            return (double) ((Unsigned64) valueObj).getVal();
        } else if (valueObj instanceof Integer16) {
            return (double) ((Integer16) valueObj).getVal();
        } else if (valueObj instanceof Integer32) {
            return (double) ((Integer32) valueObj).getVal();
        } else if (valueObj instanceof Integer64) {
            return (double) ((Integer64) valueObj).getVal();
        }
        return -1.;
    }


    private static String getString(byte[] b) {
        log.debug("Byte array: {}", b);
        StringBuilder builder = new StringBuilder();
        for (byte element : b) {
            builder.append(String.format("%02x", element));
        }
        String result = builder.toString();
        log.debug("Converted: {}", result);
        return result;
    }


    private static String getMeterId(byte[] b) {
        return convertServerIdToMeterId(getString(b));
    }


    private static String getObisCode(byte[] b) {
        assert b.length == 6;
        return String.format("%d-%d:%d.%d.%d*%d", toInt(b[0]), toInt(b[1]), toInt(b[2]), toInt(b[3]), toInt(b[4]),
                toInt(b[5]));
    }


    private static int toInt(byte b) {
        return Byte.toUnsignedInt(b);
    }


    private static String convertServerIdToMeterId(String id) {
        log.debug("Input: {}", id);

        // Sparte
        String line = id.substring(3, 4);
        log.debug("Sparte: {}", line);

        // Hersteller
        String manufacturer = new String(DatatypeConverter.parseHexBinary(id.substring(4, 10)));
        log.debug("Hersteller: {}", manufacturer);

        // Fabrikationsblock
        String serialBlock = id.substring(10, 12);
        log.debug("Fabrikationsblock: {}", serialBlock);

        // Fabrikationsnummer
        String serialNumber = String.format("%08d", Long.parseLong(id.substring(12), 16));
        log.debug("Fabrikationsnummer: {}", serialNumber);

        // Zähler-ID
        String meterId = line + manufacturer + serialBlock + serialNumber;
        log.debug("Meter-ID: {}", meterId);
        return meterId;
    }
}
