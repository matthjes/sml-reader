# SML Reader

Diese Komponente kann die Daten, welche Ã¼ber den optischen Ausgang eines Zähler bereitgestellt werden, auslesen und sowohl an einen ReST-Endpunkt als auch an einem MQTT-Broker versenden.

## Verwendete Bibliotheken

* Spring Boot
* Spring Integration
  * DSL
  * MQTT
* Apache HTTPClient
* JSML (Interpretieren des SML-Datenstromes)
* NRJavaSerial (Ansprechen der seriellen Schnittstelle)
* Bouncy Castle (Encodierung von Zählerdaten)